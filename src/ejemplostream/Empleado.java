/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplostream;

import java.io.Serializable;

public class Empleado implements Serializable{
    
    public String nombre;
    public int edad;
    public int id;
    public double salario;
    public String cargo;
    
    public Empleado(String nombre, int edad, int id, double salario, String cargo) {
        this.nombre = nombre;
        this.edad = edad;
        this.id = id;
        this.salario = salario;
        this.cargo = cargo;
    }

    Empleado() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
       
    public String [] arreglo (){
        String [] datos = {nombre, String.valueOf(edad), String.valueOf(id), 
            String.valueOf(salario), cargo};
        return datos;
    }
}
